#!/usr/bin/python
# -*- coding: utf-8 -*-
# <środa 9:30>: <Łukasz> <Zelek> (<302941>), <Adrian> <Winkler> (<302936>), <Maciej> <Wojtyś> (<302938>)

import unittest
from collections import Counter

from servers import ListServer, Product, Client, MapServer, ServerError

server_types = (ListServer, MapServer)


class ServerTest(unittest.TestCase):
    def test_get_entries_returns_proper_entries(self):
        products = [Product('P12', 1), Product('PP234', 2), Product('PP235', 1)]
        for server_type in server_types:
            server = server_type(products)
            entries = server.get_entries(2)
            self.assertEqual(Counter([products[2], products[1]]), Counter(entries))

    def test_if_data_from_get_entries_properly_sorted(self):
        products = [Product('PPP12', 1), Product('PPP234', 2), Product('PPP235', 1)]
        for server_type in server_types:
            server = server_type(products)
            entries = server.get_entries(3)
            self.assertEqual([Product('PPP12', 1), Product('PPP235', 1), Product('PPP234', 2)], entries)
        pass


class ClientTest(unittest.TestCase):
    def test_total_price_for_normal_execution(self):
        products = [Product('PP234', 2), Product('PP235', 3)]
        for server_type in server_types:
            server = server_type(products)
            client = Client(server)
            self.assertEqual(5, client.get_total_price(2))


class NoneReturnTest(unittest.TestCase):
    def test_total_price_for_normal_execution(self):
        products = [Product('PP234', 2), Product('PP235', 3), Product('PP236', 3), Product('PP233', 1)]
        for server_type in server_types:
            server = server_type(products)
            client = Client(server)
            self.assertEqual(None, client.get_total_price(2))

class ErrorTest(unittest.TestCase):
    def test_total_price_for_normal_execution(self):
        products = [Product('PP234', 2), Product('PP235', 3), Product('PP236', 3), Product('PP233', 1)]
        for server_type in server_types:
            server = server_type(products)
            client = Client(server)
            self.assertRaises(ServerError, client.get_total_price(2))


if __name__ == '__main__':
    unittest.main()

