# <środa 9:30>: <Łukasz> <Zelek> (<302941>), <Adrian> <Winkler> (<302936>), <Maciej> <Wojtyś> (<302938>)
import re
from abc import ABC
from abc import abstractmethod
from typing import List
from typing import Optional
from typing import TypeVar


class Product:
    def __init__(self, name: str, price: float) -> None:
        self.price = price
        self.name = name

    price: float = 0
    name: str = ""

    def __hash__(self):
        return hash((self.name, self.price))

    def __eq__(self, other):
        return self.name == other.name and self.price == other.price


class ServerError(Exception):
    pass


class TooManyProductsFoundError(ServerError):
    pass


class AbstractServer(ABC):
    @abstractmethod
    def get_all_entries(self, n_letters: int = 0) -> List[Product]:
        raise NotImplementedError

    def get_entries(self, n_letters: int = 0) -> List[Product]:
        list_of_find_product: List[Product] = self.get_all_entries(n_letters)

        if len(list_of_find_product) > self.n_max_returned_entries:
            raise TooManyProductsFoundError

        return sorted(list_of_find_product, key=lambda product: product.price)

    n_max_returned_entries: int = 3


class ListServer(AbstractServer):
    def __init__(self, list_of_products: List[Product]) -> None:
        super().__init__()
        self.products = []
        for product in list_of_products:
            self.products.append(product)

    def get_all_entries(self, n_letters: int = 0) -> List[Product]:
        list_of_find_product: List = []
        for product in self.products:
            if len("".join(re.findall("^[a-zA-Z]{{{n}}}\\d{{2,3}}$".format(n=n_letters), product.name))) > 0:
                list_of_find_product.append(product)
        return list_of_find_product


class MapServer(AbstractServer):
    def __init__(self, list_of_products: List[Product]) -> None:
        super().__init__()
        self.products = {}
        for product in list_of_products:
            self.products[product.name] = product

    def get_all_entries(self, n_letters: int = 0) -> List[Product]:
        list_of_find_product: List[Product] = []
        for product in self.products.values():
            if len("".join(re.findall("^[a-zA-Z]{{{n}}}\\d{{2,3}}$".format(n=n_letters), product.name))) > 0:
                list_of_find_product.append(product)
        return list_of_find_product


class Client:
    HelperType = TypeVar('HelperType', bound=AbstractServer)

    def __init__(self, server: HelperType) -> None:
        self._server = server

    def get_total_price(self, n_letters: Optional[int]) -> Optional[float]:
        try:
            list_of_results = self._server.get_entries(n_letters)
        except TooManyProductsFoundError:
            return None

        if len(list_of_results) == 0:
            return None

        total_price: float = 0
        for elem in list_of_results:
            total_price += elem.price
        return total_price
